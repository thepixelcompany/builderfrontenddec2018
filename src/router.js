import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Creator from './views/Creator.vue'
import Preview from './views/Preview.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/creator',
      name: 'creator',
      component: Creator
    },
    {
      path: '/preview',
      name: 'preview',
      component: Preview
    }
  ]
})
