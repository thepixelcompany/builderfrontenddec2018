var widgets
const install = (Vue, config = {}) => {
  widgets = config.widgets
  widgets.forEach(widget => {
    widget.items.forEach(item => {
      Object.keys(item).forEach(key => {
        Vue.component(key, item[key])
      })
    })
  })
}

export default {
  install,
  getWidgets () {
    return widgets
  }
}
