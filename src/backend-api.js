// Author: Khuyag
import axios from 'axios'
/**
 * Creates an axios instance, defines base URL and a header type.
 * @type {Router}
 */
export default axios.create({
  baseURL: 'http://localhost:4000/api',
  timeout: 100000,
  headers: {
    'Content-Type': 'application/json'
  }
})

// if (localStorage.getItem('token')) {
//     axios.defaults.headers.common['X-Auth'] = localStorage.getItem('token');
// }
