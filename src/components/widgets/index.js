import Section from './basic/section/index.vue'
import Container from './basic/container/index.vue'
import DivBlock from './basic/div-block/index.vue'
import TwoColumns from './basic/2-column/index.vue'
import ThreeColumns from './basic/3-column/index.vue'
import TwoSevenColumns from './basic/2-7-column/index.vue'
import Button from './basic/button/index.vue'
import Link from './basic/link/index.vue'
import List from './basic/list/index.vue'
/*************************************************************/
import Heading from './typography/heading/index.vue'
import Paragraph from './typography/paragraph/index.vue'
import Quote from './typography/quote/index.vue'
import RichText from './typography/rich-text/index.vue'
import TextBlock from './typography/text-block/index.vue'
/*************************************************************/
import NavBar from './extras/navbar/index.vue'
import Dropdown from './extras/dropdown/index.vue'
import Slider from './extras/slider/index.vue'
import TabModule from './extras/tab-module/index.vue'
import Image from './extras/image/index.vue'
import VideoPlayer from './extras/video-player/index.vue'
/*************************************************************/
import FormBlock from './forms/form-block/index.vue'
import Input from './forms/input/index.vue'
import TextArea from './forms/textarea/index.vue'
import FormLable from './forms/form-label/index.vue'
import Selector from './forms/selector/index.vue'
import Radio from './forms/radio/index.vue'
import CheckMark from './forms/checkbox/index.vue'

export default {
  widgets: [{
    label: 'Basic',
    items: [
      {
        [Section.name]: Section,
        [Container.name]: Container,
        [DivBlock.name]: DivBlock
      }, {
        [TwoColumns.name]: TwoColumns,
        [ThreeColumns.name]: ThreeColumns,
        [TwoSevenColumns.name]: TwoSevenColumns
      }, {
        [Button.name]: Button,
        [Link.name]: Link,
        [List.name]: List
      }
    ]
  }, {
    label: 'Typography',
    items: [
      {
        [Heading.name]: Heading,
        [Paragraph.name]: Paragraph,
        [Quote.name]: Quote
      }, {
        [RichText.name]: RichText,
        [TextBlock.name]: TextBlock
      }
    ]
  }, {
    label: 'Extra',
    items: [
      {
        [NavBar.name]: NavBar,
        [Dropdown.name]: Dropdown,
        [Slider.name]: Slider
      }, {
        [TabModule.name]: TabModule,
        [Image.name]: Image,
        [VideoPlayer.name]: VideoPlayer
      }
    ]
  }, {
    label: 'Forms',
    items: [
      {
        [FormBlock.name]: FormBlock,
        [Input.name]: Input,
        [TextArea.name]: TextArea
      }, {
        [FormLable.name]: FormLable,
        [Selector.name]: Selector,
        [Radio.name]: Radio
      }, {
        [CheckMark.name]: CheckMark
      }
    ]
  }]
}
