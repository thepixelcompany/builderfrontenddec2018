import Vue from 'vue'
import Vuex from 'vuex'
import backend_api from './backend-api.js'
import eventBus from './event-bus.js'
import Axios from 'axios'
const generate = require('nanoid/generate')
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    spinner: false,
    isEdit: 1,
    htmlCode: '',
    cssCode: '',
    jsCode: '',
    previewMode: 1,
    showAppStore: 0,
    showLogoMenu: 0,
    showChatModal: 0,
    showCodeModal: 0,
    showPublishMenu: 0,
    showPageDrawer: 0,
    showComingSoonModal: 0,
    showMediaDrawer: 0,
    uuid: null,
    widgets: [],
    classes: [],
    activeWidget: {}
  },
  /***************************************************************************************/
  mutations: {
    addWidget (state, { data: data = null, params }) {
      let where
      if (params.where === undefined) {
        where = null
      } else {
        where = params.where
      }
      let def = {}
      if (params.order !== undefined) {
        def = { uuid: 'id_' + generate('1234567890abcedf', 8), parent: params.parent, where: where, order: params.order }
      } else {
        def = { uuid: 'id_' + generate('1234567890abcdef', 8), parent: params.parent, where: where }
      }
      let setting = params.item
      if (data !== null) {
        setting.src = data
      }
      state.widgets.push(Object.assign(setting, def))
      if (setting.type === 'rich-text-comp') {
        params.childItems.forEach(i => {
          let def = { uuid: 'id_' + generate('1234567890abcdef', 8), parent: setting.uuid, where: where }
          state.widgets.push(Object.assign(i, def))
        })
      }
    },
    select (state, payload) {
      state.uuid = payload.uuid
      let widget = state.widgets.find(i => i.uuid === state.uuid)
      if (state.uuid !== -1) {
        state.activeWidget = widget
        state.activeWidget.zIndex = 1
      } else {
        state.activeWidget = {}
      }
    },
    clone (state) {
      let clone = Object.assign({}, state.activeWidget, { uuid: 'id_' + generate('1234567890abcdef', 8) })
      state.widgets.push(clone)
    },
    remove (state) {
      state.widgets.forEach((i, j) => {
        if (i.uuid === state.activeWidget.uuid) {
          state.widgets.splice(j, 1)
          state.uuid = -1
        }
      })
    },
    updateWidget (state, payload) {
      state.activeWidget.parent = payload.params.parent
      state.activeWidget.where = payload.params.where
    },
    sortWidget (state, params) {
      let widgets = state.widgets.slice(0, state.widgets.length)
      let index1 = widgets.findIndex(i => i.uuid === params.item.uuid)
      let originindex2 = widgets.findIndex(i => i.uuid === params.after.uuid)
      widgets.splice(index1, 1)
      let index2 = widgets.findIndex(i => i.uuid === params.after.uuid)
      if (index1 > originindex2) {
        widgets.splice(index2, 0, params.item)
      } else {
        widgets.splice(index2 + 1, 0, params.item)
      }
      state.widgets = widgets
    },
    updateData (state, params) {
      let element = state.widgets.find(i => i.uuid === params.id)
      element[params.key] = params.value
    },
    addClass (state, payload) {
      let className = { name: payload, noneStyle: {}, hoverStyle: {}, focusStyle: {} }
      state.activeWidget.class.push(payload)
      state.classes.push(className)
    },
    removeClass (state) {
      state.activeWidget.class.splice(0, 1)
    },
    setClass (state, payload) {
      let classObj = state.classes.find(i => i.name === payload.name)
      let newStyle = {}
      if(classObj !== undefined) {
        newStyle[payload.styleType] = Object.assign({}, classObj[payload.styleType], payload.value)
        classObj = Object.assign(classObj, newStyle)
      } else {
        eventBus.$emit('styleWarning');
      }
    }
  },
  /*******************************************************************************************/
  actions: {
    addWidget ({ state, commit, store }, params) {
      if (params.item.isUpload) {
        eventBus.$emit('upload', (payload) => {
          commit('addWidget', { data: payload, params })
          commit('select', { uuid: state.widgets[state.widgets.length - 1].uuid })
        }, true)
      } else {
        commit('addWidget', { params })
        commit('select', { uuid: state.widgets[state.widgets.length - 1].uuid })
      }
    },
    updateWidget ({ state, commit }, params) {
      commit('updateWidget', { params })
    },
    unselect ({ state, commit, store }) {
      commit('select', { uuid: -1 })
    },
    uploadHandle ({ state, commit }, payload) {
      return new Promise((resolve, reject) => {
        backend_api.post('uploads', payload, { headers: { 'Content-Type': 'multipart/form-data' } }).then(res => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
      })
    },
    uploadMediaHandle ({ state, commit }, payload) {
      return new Promise((resolve, reject) => {
        backend_api.post('media/insert', payload, { headers: { 'Content-Type': 'multipart/form-data' } }).then(res => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
      })
    },
    getMediaHandle ({ state, commit }) {
      return new Promise((resolve, reject) => {
        backend_api.get('media/get').then(res => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
      })
    },
    deleteMediaHandle ({ state, commit }, payload) {
      return new Promise((resolve, reject) => {
        backend_api.post('media/delete', payload).then(res => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
      })
    },
    fileWrite ({ state, commit }, payload) {
      if (payload.type) {
        return new Promise((resolve, reject) => {
          backend_api.post('preview', payload).then(res => {
            resolve(res)
          }).catch(err => {
            reject(err)
          })
        })
      } else {
        return false
      }
    },
    getHtml ({ state, commit }) {
      return new Promise((resolve, reject) => {
        Axios.get('http://localhost:4000/src/index.html').then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    getCss ({ state, commit }) {
      return new Promise((resolve, reject) => {
        Axios.get('http://localhost:4000/src/style.css').then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    getJs ({ state, commit }) {
      return new Promise((resolve, reject) => {
        Axios.get('http://localhost:4000/src/main.js').then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    updateCodeHandle ({ state, commit }, payload) {
      return new Promise((resolve, reject) => {
        backend_api.post('preview/update', payload).then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
})
